﻿using Moq;
using NUnit.Framework;
using SimulacroFinal.Controllers;
using SimulacroFinal.Models.Entidades;
using SimulacroFinal.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimulacroFinal.Test
{
    [TestFixture]
    public class Test
    {
        Mock<IEventoService> _serviceEvento;
        Mock<IAreaService> _serviceArea;

        EventoController eventoController;

        [SetUp]
        public void SetUp()
        {
            _serviceEvento = new Mock<IEventoService>();
            _serviceArea = new Mock<IAreaService>();

            eventoController = new EventoController(_serviceEvento.Object, _serviceArea.Object);
        }

        [Test]
        public void TestFecha()
        {
            var result = eventoController.Create
                (
                    new Evento
                    {
                        Fecha = DateTime.Now.Date.AddDays(1)
                    }    
                ) as ViewResult;

            Assert.AreEqual(true, result.ViewData.ModelState.ContainsKey("Fecha"));
        }

        public void MockearMetodos()
        {
            _serviceArea.Setup(o => o.GetById(It.IsAny<int>())).Returns
                (
                    new Area
                    {
                        Responsable = "waa";
                    }
                );
        }
    }
}
