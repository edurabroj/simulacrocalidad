﻿using SimulacroFinal.Models.Entidades;
using SimulacroFinal.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimulacroFinal.Controllers
{
    public class EventoController : Controller
    {
        private IEventoService _serviceEvento;
        private IAreaService _serviceArea;

        public EventoController
        (
            IEventoService _serviceEvento,
            IAreaService _serviceArea
        )
        {
            this._serviceEvento = _serviceEvento;
            this._serviceArea = _serviceArea;
        }

        // GET: Evento
        public ActionResult Index()
        {
            return View(_serviceEvento.GetAll());
        }

        public ActionResult Details(int id)
        {
            return View(_serviceEvento.GetById(id));
        }

        public ActionResult Create()
        {
            LlenarCombos();
            return View();
        }        

        [HttpPost]
        public ActionResult Create(Evento model)
        {
            Validar(model);
            if(ModelState.IsValid)
            {
                if(string.IsNullOrEmpty(model.Responsable))
                {
                    model.Responsable = _serviceArea.GetById(model.AreaId).Responsable;
                }

                _serviceEvento.Create(model);
                return RedirectToAction("Index");
            }
            LlenarCombos();
            return View();
        }
        
        public void LlenarCombos()
        {
            var lista = new List<string>();
            lista.Add("Caxas");
            lista.Add("San Andreas");
            ViewBag.Lugares = new SelectList(lista);

            ViewBag.Areas = new SelectList(_serviceArea.GetAll(),"AreaId","Nombre");
        }

        private void Validar(Evento model)
        {
            if(model.Fecha.Date>DateTime.Now.Date)
            {
                ViewData.ModelState.AddModelError("Fecha", "Fecha futura no vale pe");
            }
            if (string.IsNullOrEmpty(model.Reportero))
            {
                ViewData.ModelState.AddModelError("Reportero", "Campo obligatorio");
            }
        }
    }
}