﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimulacroFinal.Models.Entidades
{
    public class Area
    {
        public int AreaId { get; set; }
        public string Nombre { get; set; }
        public string Lugar { get; set; }
        public string Responsable { get; set; }
    }
}