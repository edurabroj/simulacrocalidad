﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimulacroFinal.Models.Entidades
{
    public class Evento
    {
        public int EventoId { get; set; }
        public string Descripcion { get; set; }
        public string Reportero { get; set; }
        public Area Area { get; set; }
        public int AreaId { get; set; }
        //no futura
        public DateTime Fecha { get; set; }
        //el del area si es vacio
        public string Responsable { get; set; }
        public string Lugar { get; set; }
    }
}