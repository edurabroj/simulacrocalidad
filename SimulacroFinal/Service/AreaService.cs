﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SimulacroFinal.Models.Entidades;

namespace SimulacroFinal.Service
{
    public class AreaService : IAreaService
    {
        Context context;

        public AreaService()
        {
            context = new Context();
        }

        public List<Area> GetAll()
        {
            return context.Areas.ToList();
        }

        public Area GetById(int id)
        {
            return context.Areas.Find(id);
        }
    }
}