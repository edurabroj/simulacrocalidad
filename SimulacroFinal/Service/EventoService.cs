﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SimulacroFinal.Models.Entidades;

namespace SimulacroFinal.Service
{
    public class EventoService : IEventoService
    {
        private Context context;

        public EventoService()
        {
            context = new Context();
        }

        public void Create(Evento model)
        {
            context.Eventos.Add(model);
            context.SaveChanges();
        }

        public List<Evento> GetAll()
        {
            return context.Eventos.ToList();
        }

        public Evento GetById(int id)
        {
            return context.Eventos.Find(id);
        }
    }
}