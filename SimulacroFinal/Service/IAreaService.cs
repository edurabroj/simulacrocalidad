﻿using SimulacroFinal.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulacroFinal.Service
{
    public interface IAreaService
    {
        List<Area> GetAll();
        Area GetById(int id);
    }
}
