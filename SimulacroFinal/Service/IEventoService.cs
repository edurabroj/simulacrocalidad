﻿using SimulacroFinal.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulacroFinal.Service
{
    public interface IEventoService
    {
        List<Evento> GetAll();
        void Create(Evento model);
        Evento GetById(int id);
    }
}
