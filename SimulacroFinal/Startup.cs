﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SimulacroFinal.Startup))]
namespace SimulacroFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
